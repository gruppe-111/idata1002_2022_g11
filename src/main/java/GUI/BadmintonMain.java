package GUI;

import GUI.Views.BadmintonApp;

/**
 * The main starting point for the application.
 */
public class BadmintonMain {

    /**
     * Main starting point for the application.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        BadmintonApp.main(args);
    }
}
