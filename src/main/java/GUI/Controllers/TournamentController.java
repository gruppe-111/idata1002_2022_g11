package GUI.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class TournamentController extends GeneralController {

    // Fields mapped to the FXML-file.
    @FXML
    private BorderPane TournamentBorderPane;
    @FXML
    private Button GenerateTournament_sub;
    @FXML
    private Button UpcomingTournament_sub;
    @FXML
    private Button PlayedTournament_sub;

    private BorderPane borderPane;


    //----------------- GENERAL CLASSES  __________________
    /**
     * allows for the highlight of the currently selected submenu button
     */
    @Override
    protected void subMenuStyleControl(){
        UpcomingTournament_sub.getStyleClass().clear();
        GenerateTournament_sub.getStyleClass().clear();
        PlayedTournament_sub.getStyleClass().clear();
        UpcomingTournament_sub.getStyleClass().add("Unselected_sub");
        PlayedTournament_sub.getStyleClass().add("Unselected_sub");
        GenerateTournament_sub.getStyleClass().add("Unselected_sub");
    }

    //----------------- MAIN MENU BUTTONS __________________


    /**
     * switches the scene to Players
     * @param event the event sent from the Players button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickPlayers(ActionEvent event) throws IOException {
        super.path ="Player/MainWindowPlayers.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Matches
     * @param event the event sent from the Matches button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickMatches(ActionEvent event) throws IOException {
        super.path ="Match/MatchesTable.fxml";
        super.switchMainMenu(event, path);
    }


    @FXML
    private void onClickTournaments(ActionEvent event) throws IOException {

    }
    @FXML
    private void onClickResults(ActionEvent event) throws IOException {

    }

    /**
     * switches the scene to Classes
     * @param event the event sent from the Classes button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickClasses(ActionEvent event) throws IOException {
        super.path ="Class/Classes.fxml";
        super.switchMainMenu(event, path);
    }

    //----------------- SUB MENU BUTTONS __________________



    /**
     * replaces the content in the borderpane to match the desired content in Played Tournament submenu
     * @param event the event sent from the PlayedTournament_sub button
     * @throws IOException throws an exception if there no path provided
     */
    @FXML
    private void onClickPlayedTournament_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Tournament/PlayedTournament.fxml";
        String rightContentPath = null;
        Button button = PlayedTournament_sub;
        borderPane = TournamentBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired content in Upcoming Tournament submenu
     * @param event the event sent from the UpcomingTournament_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickUpcomingTournament_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Tournament/UpcomingTournament.fxml";
        String rightContentPath = null;
        Button button = UpcomingTournament_sub;
        borderPane = TournamentBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);

    }

    /**
     * replaces the content in the borderpane to match the desired content in Generate Tournament submenu
     * @param event the event sent from the GenerateTournament_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickGenerateTournament_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Tournament/TableGenerateTournament.fxml";
        String rightContentPath = null;
        Button button = GenerateTournament_sub;
        borderPane = TournamentBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }


    //----------------- MAIN CONTENT BUTTONS __________________



    @FXML
    private void onClickRegisterTournament_button(ActionEvent event) throws IOException {

    }

}
