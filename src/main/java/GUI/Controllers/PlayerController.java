package GUI.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class PlayerController extends GeneralController{


    // Fields mapped to the FXML-file.

    @FXML
    private BorderPane PlayerBorderPane;
    @FXML
    private Button RegisterPlayer_button;
    @FXML
    private Button RegisterPlayer_sub;
    @FXML
    private Button Classes_sub;
    @FXML
    private Button PlayerTable_sub;
    @FXML
    private Button SearchPlayer_sub;

    private BorderPane borderPane;




    //----------------- GENERAL CLASSES  __________________
    /**
     * allows for the highlight of the currently selected submenu button
     */
    @FXML
    @Override
    protected void subMenuStyleControl(){
        RegisterPlayer_sub.getStyleClass().clear();
        PlayerTable_sub.getStyleClass().clear();
        Classes_sub.getStyleClass().clear();
        SearchPlayer_sub.getStyleClass().clear();
        PlayerTable_sub.getStyleClass().add("Unselected_sub");
        RegisterPlayer_sub.getStyleClass().add("Unselected_sub");
        Classes_sub.getStyleClass().add("Unselected_sub");
        SearchPlayer_sub.getStyleClass().add("Unselected_sub");
}

    //----------------- MAIN MENU BUTTONS __________________


    @FXML
    private void onClickPlayers(ActionEvent event) throws IOException {

    }

    /**
     * switches the scene to Matches
     * @param event the event sent from the Matches button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickMatches(ActionEvent event) throws IOException {
        super.path ="Match/MatchesTable.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Tournaments
     * @param event the event sent from the Tournaments button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickTournaments(ActionEvent event) throws IOException {
        super.path ="Tournament/GenerateTournament.fxml";
        super.switchMainMenu(event, path);
    }
    /**
     * switches the scene to Classes
     * @param event the event sent from the Classes button
     * @throws IOException throws an exception if there no path provided
     */
    @FXML
    private void onClickClasses(ActionEvent event) throws IOException {
        super.path ="Class/Classes.fxml";
        super.switchMainMenu(event, path);
    }
    @FXML
    private void onClickResults(ActionEvent event) throws IOException {

    }


    //----------------- SUB MENU BUTTONS  __________________


    /**
     * replaces the content in the borderpane to match the desired content in Player Table submenu
     * @param event the event sent from the PlayerTable_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickPlayerTable_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Player/TablePlayer.fxml";
        String rightContentPath = null;
        Button button = PlayerTable_sub;
        borderPane = PlayerBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired content in Register Player submenu
     * @param event the event sent from the RegisterPlayer_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void RegisterPlayer_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Player/RegisterPlayer.fxml";
        String rightContentPath = null;
        Button button = RegisterPlayer_sub;
        borderPane = PlayerBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);

    }

    /**
     * replaces the content in the borderpane to match the desired content in Classes submenu
     * @param event the event sent from the Classes_sub button
     * @throws IOException throw an exception if there no path provided
     */

    @FXML
    private void onClickClasses_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Player/Classes.fxml";
        String rightContentPath = null;
        Button button = Classes_sub;
        borderPane = PlayerBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired content in Search Player submenu
     * @param event the event sent from the SearchPlayer_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickSearchPlayer_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Player/SearchPlayer.fxml";
        String rightContentPath = null;
        Button button = SearchPlayer_sub;
        borderPane = PlayerBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }


    //----------------- MAIN CONTENT BUTTONS __________________
    /**
     * replace the button text with clicked 'Clicked!'
     * @param event the event sent from the Player_button button
     * @throws IOException throws an exception if there no path provided
     */
    @FXML
    private void onClickRegisterPlayer_button(ActionEvent event) throws IOException {
    RegisterPlayer_button.setText("Clicked!");
    }


}

