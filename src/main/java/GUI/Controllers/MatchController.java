package GUI.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.io.IOException;


public class MatchController extends GeneralController{

    // Fields mapped to the FXML-file.
    @FXML
    private Button RegisterMatch_sub;
    @FXML
    private Button OngoingMatch_sub;
    @FXML
    private Button MatchTable_sub;
    @FXML
    private Button SearchMatch_sub;
    @FXML
    private BorderPane MatchBorderPane;

    protected BorderPane borderPane;




    //----------------- GENERAL CLASSES  __________________
    /**
     * allows for the highlight of the currently selected submenu button
     */
    protected void subMenuStyleControl(){
        RegisterMatch_sub.getStyleClass().clear();
        OngoingMatch_sub.getStyleClass().clear();
        SearchMatch_sub.getStyleClass().clear();
        MatchTable_sub.getStyleClass().clear();
        MatchTable_sub.getStyleClass().add("Unselected_sub");
        SearchMatch_sub.getStyleClass().add("Unselected_sub");
        OngoingMatch_sub.getStyleClass().add("Unselected_sub");
        RegisterMatch_sub.getStyleClass().add("Unselected_sub");
    }


    //----------------- MAIN MENU BUTTONS __________________

    /**
     * switches the scene to Players
     * @param event the event sent from the Players button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickPlayers(ActionEvent event) throws IOException {
        super.path ="Player/MainWindowPlayers.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Matches
     * @param event the event sent from the Matches button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickMatches(ActionEvent event) throws IOException {
        super.path ="Match/MatchesTable.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Tournaments
     * @param event the event sent from the Tournaments button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickTournaments(ActionEvent event) throws IOException {
        super.path ="Tournament/GenerateTournament.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Classes
     * @param event the event sent from the Classes button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickClasses(ActionEvent event) throws IOException {
        super.path ="Class/Classes.fxml";
        super.switchMainMenu(event, path);
    }
    @FXML
    private void onClickResults(ActionEvent event) throws IOException {

    }

    //----------------- SUB MENU BUTTONS  __________________


    /**
     * replaces the content in the borderpane to match the desired content in Matches Table submenu
     * @param event the event sent from the MatchesTable_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickMatchTable_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Match/SearchMatch.fxml";
        String rightContentPath = "Match/RightMatchesTable.fxml";
        Button button = MatchTable_sub;
        borderPane = MatchBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired content in Search Match submenu
     * @param event the event sent from the SearchMatch_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickSearchMatch_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Match/SearchMatch.fxml";
        String rightContentPath = "Match/RightSearchMatch.fxml";
        Button button = SearchMatch_sub;
        borderPane = MatchBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired content in Ongoing Match submenu
     * @param event the event sent from the OngoingMatch_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickOngoingMatch_sub(ActionEvent event) throws IOException {

        String mainContentPath = "Match/OngoingMatches.fxml";
        String rightContentPath = null;
        Button button = OngoingMatch_sub;
        borderPane = MatchBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired content in Register Match  submenu
     * @param event the event sent from the RegisterMatch_sub button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickRegisterMatch_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Match/RegisterMatch.fxml";
        String rightContentPath = null;
        Button button = RegisterMatch_sub;
        borderPane = MatchBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }




}
