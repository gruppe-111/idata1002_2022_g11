package GUI.Controllers;

import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *This class has methods that load all parts of the GUI page. all controller classes inherit from this class.
 *
 */
public abstract class GeneralController {


    // Fields mapped to the FXML-file.
    @FXML
     protected BorderPane ClassesBorderPane;

    private Stage stage;
    private Scene scene;
    private Parent root;
    private SplitPane mainContent;
    private HBox rightSideContent;
    //
    protected String path ="Class/Classes";

    /**
     * replaces the borderpane center content with the content specified in the fileName
     * @param fileName the url/path of the content to be loaded
     * @return returns a JavaFX SplitPane
     */
    public SplitPane getMainContent(String fileName) {
        try {

            URL fileUrl =getClass().getResource("/ui/"+fileName);
            if(fileUrl==null) {
                throw new java.io.FileNotFoundException("FXML file could not be found");

            }


            mainContent = new FXMLLoader().load(fileUrl);
        } catch (Exception e) {
            System.out.println("No page " +"'"+fileName+"'"+ "found, check FxmlLoader");
            return null;
        }
        return mainContent;
    }

    /**
     * replaces the borderpane right content with the content specified in the fileName
     * @param fileName the url/path of the content to be loaded
     * @return returns a JavaFX HBox
     */
    public HBox getRightSideContent(String fileName) {
        try {

            URL fileUrl =getClass().getResource("/ui/"+fileName);
            if(fileUrl==null) {
                throw new java.io.FileNotFoundException("FXML file could not be found");

            }

            rightSideContent = new FXMLLoader().load(fileUrl);
        } catch (Exception e) {
            return null;
        }
        return rightSideContent;
    }


    /**
     * switches scene when user click on a main menu button
     * @param event the event sent from the main menu
     * @param menuPath the path of the scene to be loaded
     * @throws IOException throws an exception if there no path provided
     */
    protected void switchMainMenu(ActionEvent event, String menuPath) throws IOException {
        URL url = getClass().getResource("/ui/"+menuPath);
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        Parent root = fxmlLoader.load();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/CSS/styles.css").toExternalForm());

        stage.setScene(scene);
        stage.show();
    }

    /**
     * loads up the new content without switching scene
     * @param mainContentPath the url/ path of the ui page for borderpane center content
     * @param rightContentPath the url/ path of the ui page for borderpane right content
     * @param button the button to be highlighted
     * @param borderPane the borderpane to adjust
     */
        protected void switchContent(String mainContentPath, String rightContentPath, Button button, BorderPane borderPane) {
            SplitPane mainContent = getMainContent(mainContentPath);
            HBox rightSideContent = getRightSideContent(rightContentPath);

            if(rightSideContent != null) {
                borderPane.setRight(rightSideContent);
            } else {
                borderPane.setRight(null);
            }

            if(mainContent !=null) {
                borderPane.setCenter(mainContent);
                subMenuStyleControl();
                button.getStyleClass().add("Selected_sub");

            }
        }


    protected abstract void subMenuStyleControl();
}
