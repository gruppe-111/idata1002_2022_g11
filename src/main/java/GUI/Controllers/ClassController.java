package GUI.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.io.IOException;

public class ClassController extends GeneralController {

    // Fields mapped to the FXML-file.
    @FXML
    private BorderPane ClassesBorderPane;
    @FXML
    private Button ClassesClasses_sub;
    @FXML
    private Button SearchClass_sub;
    private BorderPane borderPane;


    //----------------- GENERAL CLASSES  __________________

    /**
     * allows for the highlight of the currently selected submenu button
     */
    @Override
    protected void subMenuStyleControl(){
        ClassesClasses_sub.getStyleClass().clear();
        SearchClass_sub.getStyleClass().clear();
        ClassesClasses_sub.getStyleClass().add("Unselected_sub");
        SearchClass_sub.getStyleClass().add("Unselected_sub");
    }

   //----------------- MAIN MENU BUTTONS __________________

    /**
     * switches the scene to Players
     * @param event the event sent from the Players button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickPlayers(ActionEvent event) throws IOException {
        super.path ="Player/MainWindowPlayers.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Matches
     * @param event the event sent from the Matches button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickMatches(ActionEvent event) throws IOException {
        super.path ="Match/MatchesTable.fxml";
        super.switchMainMenu(event, path);
    }

    /** switches the scene to Tournaments
     * @param event the event sent from the Tournaments button
     * @throws IOException throws an exception if there no path provided
     */

    @FXML
    private void onClickTournaments(ActionEvent event) throws IOException {
        super.path ="Tournament/GenerateTournament.fxml";
        super.switchMainMenu(event, path);
    }

    /**
     * switches the scene to Classes
     * @param event the event sent from the Classes button
     * @throws IOException throws an exception if there no path provided
     */
    @FXML
    private void onClickClasses(ActionEvent event) throws IOException {
        super.path ="Class/Classes.fxml";
        super.switchMainMenu(event, path);
    }
    @FXML
    private void onClickResults(ActionEvent event) throws IOException {

    }


    //----------------- SUB MENU BUTTONS __________________

    /**
     * replaces the content in the borderpane to match the desired content in classes submenu
     * @param event the event sent from the Classes_sub button
     * @throws IOException throws an exception if there no path provided
     */
    @FXML
    private void onClickClasses_sub(ActionEvent event) throws IOException{

        String mainContentPath = "Class/TableClasses.fxml";
        String rightContentPath = "Class/RightClasses.fxml";
        Button button = ClassesClasses_sub;
        borderPane = ClassesBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

    /**
     * replaces the content in the borderpane to match the desired in search classes submenu
     * @param event the event sent from the Classes_sub button
     * @throws IOException throws an exception if there no path provided
     */
    @FXML
    private void onClickSearchClass_sub(ActionEvent event) throws IOException {
        String mainContentPath = "Class/SearchClass.fxml";
        String rightContentPath = null;
        Button button = SearchClass_sub;
        borderPane = ClassesBorderPane;
        switchContent(mainContentPath,rightContentPath,button,borderPane);
    }

}
