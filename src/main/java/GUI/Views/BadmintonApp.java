package GUI.Views;
import java.net.URL;
import java.util.logging.Logger;

import GUI.Controllers.PlayerController;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class BadmintonApp extends Application{

    private Logger logger = Logger.getLogger(getClass().getName());

    public static void main(String[] args) {
        launch(args);
    }
    @FXML
    private Button PlayerTable_sub;
    @Override
    public void start(Stage primaryStage) {
        try {
            URL url =getClass().getResource("/ui/Player/MainWindowPlayers.fxml");

            FXMLLoader fxmlLoader = new FXMLLoader(url);
            Parent root = fxmlLoader.load();
            // NB! You MUST load the FXML-file BEFORE you ask for the controller.
            PlayerController playerController = fxmlLoader.getController();
            // Now you have access to the controller instance, and can transfer objects to the controller
            Scene scene = new Scene(root, 1700, 1000);
            scene.getStylesheets().add(getClass().getResource("/CSS/styles.css").toExternalForm());
            primaryStage.setTitle("CUP2000");
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            logger.severe("ERROR:"  + e.getMessage());
        }
    }

}