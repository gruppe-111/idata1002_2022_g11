package Exceptions;

/**
 * Exception for file already exists in directory.
 *
 * @author Jakob Finstad
 * @version 16.04.2022
 */
public class FileAlreadyExists extends Exception{
    private String message;

    /**
     * Constructor for this exception. Sets the message to predefined text.
     */
    public FileAlreadyExists(){
        this.message = "The file with the name already exists on the location.";
    }

    /**
     * Get the message from this exception.
     *
     * @return the message for this exception
     */
    public String getMessage(){
        return this.message;
    }
}
