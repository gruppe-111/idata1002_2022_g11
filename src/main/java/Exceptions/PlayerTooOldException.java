package Exceptions;

/**
 * Exception for trying to add a player with higher age than maximum age.
 *
 * @author Jakob Finstad
 * @version 25.03.2022
 */
public class PlayerTooOldException extends Exception {
    private String message;
    private int playerNumber;

    /**
     * Constructor.
     */
    public PlayerTooOldException(int playerNumber){
        message = "One of the players you wanted to register is too old";
        this.playerNumber = playerNumber;
    }

    /**
     * Get the message from this exception.
     *
     * @return the exception message
     */
    public String getMessage(){
        return this.message;
    }

    /**
     * Get the number of the player.
     *
     * @return the number of the player
     */
    public int getPlayerNumber(){
        return this.playerNumber;
    }
}
