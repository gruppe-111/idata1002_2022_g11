package Logging;

import Data.Player;
import Exceptions.FileAlreadyExists;

import java.io.*;
import java.util.List;

/**
 * Class to manage save players to file and to create new file for the players you want to save.
 *
 * @author Jakob Finstad
 * @version 16.04.2022
 */
public class PlayerSaver {
    private FileWriter fileWriter;
    private PrintWriter printWriter;


    /**
     * Create a file with the name given by user. Throws exception if the file already exists.
     *
     * @param nameOfFile the name of the file you want to create
     * @throws FileAlreadyExists throws if the file already exists in the directory
     */
    public void createFile(String nameOfFile) throws FileAlreadyExists {

        try {
            File playerFile = new File(nameOfFile + ".txt");
            if(playerFile.createNewFile()){
                System.out.println("File created: "+ playerFile.getName());
            }else{
               throw new FileAlreadyExists();
            }
        } catch (IOException e){
            System.out.println("Something went wrong");
            System.out.println(e.getMessage());
        }
    }

    /**
     * Save player info to a file. Throws if the file could not be found. And returns true if the info was written to file.
     *
     * @param players list of players that shall be saved in the file
     * @param file the file that players shall be saved to
     * @return true if the players was saved to the file
     */
    public boolean savePlayersTooFile(List<Player> players, File file){
        boolean writtenToFile = false;
        try{
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);

            for (int i = 0; i <players.size();i++) {
                Player p = players.get(i);
                String playerString =
                        p.getFirstName() + "," +
                        p.getLastName() + "," +
                        p.getAge() + "," +
                        p.getNationality() + "," +
                        p.getMatchesPlayed() + "," +
                        p.getMatchesWon();
                printWriter.println(playerString);
            }

            printWriter.close();
            writtenToFile = true;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            printWriter.close();
        }
        return writtenToFile;
    }
}
