module GruppeProsjekt {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    opens GUI.Controllers to javafx.fxml, javafx.graphics;
    opens GUI.Views to javafx.fxml, javafx.graphics;
    opens Exceptions;
    opens GUI;
    opens Data;
    exports GUI;
    exports Data;
    exports Exceptions;


}