package Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A team with players.
 *
 * @author Jakob Finstad
 * @version 25.03.2022
 */
public class Team {
    private String name;
    private String location;
    private String homeCourt;
    private List<Player> squad;


    /**
     * Create a new team. All fields are set to lowercases.
     *
     * @param name the name of the team
     * @param location the location where the team are from
     * @param homeCourt the name of the homecourt to the team
     */
    public Team(String name, String location, String homeCourt){
        this.setName(name);
        this.setLocation(location);
        this.setHomeCourt(homeCourt);
        this.squad = new ArrayList<>();
    }

    /**
     * Set the name of the team. If the name is blank, invalid input will be set as name.
     *
     * @param name the name of the team
     */
    private void setName(String name){
        if(name.isEmpty()&&name.isBlank()){
            this.name = "Invalid name";
        }else{
            this.name = name.strip().toLowerCase(Locale.ROOT);
        }
    }

    /**
     * Set the location of the team. If the location is empty of blank,
     * the location will be set to predefined value.
     *
     * @param location the location of the team
     */
    private void setLocation(String location){
        if(location.isEmpty() || location.isBlank()){
            this.location = "Invalid location";
        }else{
            this.location = location.strip().toLowerCase(Locale.ROOT);
        }
    }

    /**
     * Set the homecourt of the team. If the homecourt is blank or empty,
     * the homecourt will be set to predefined value.
     *
     * @param homeCourt the name of the homecourt to this team
     */
    private void setHomeCourt(String homeCourt){
        if (homeCourt.isBlank() || homeCourt.isEmpty()){
            this.homeCourt = "Invalid HomeCourt";
        }else{
            this.homeCourt = homeCourt.strip().toLowerCase(Locale.ROOT);
        }
    }

    /**
     * Add a player to the squad.
     *
     * @param player the player that shall be added to the team
     */
    public void addMember(Player player){
        this.squad.add(player);
    }

    /**
     * Add multiple players to the team. If the list is empty nothing will be done.
     *
     * @param players a list of the players that shall be added to the squad
     */
    public void addMultipleMembers(List<Player> players){
        if (!players.isEmpty()){
            this.squad.addAll(players);
        }
    }

    /**
     * Get the name of the team.
     *
     * @return name of the team
     */
    public String getName() {
        return name;
    }


    /**
     * Get the location of the team. Where this team comes from.
     *
     * @return the origin of the team
     */
    public String getLocation() {
        return location;
    }


    /**
     * Get the homecourt of this team. The name of the stadium this team play their home matches on.
     *
     * @return location of home matches
     */
    public String getHomeCourt() {
        return homeCourt;
    }


    /**
     * Get the squad of the team. The players that's assigned to this team.
     *
     * @return list of players on the team
     */
    public List<Player> getSquad() {
        return squad;
    }
}
