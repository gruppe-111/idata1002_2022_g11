package Data;


import Exceptions.PlayerTooOldException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Player league to player. A display of their skill group.
 *
 * @author Jakob Finstad
 * @version 25.03.2022
 */
public class PlayerLeague {
    private List<Player> players;
    private int maxAge;
    private char sex;
    private String nameOfLeague;

    /**
     * Constructor for the playerLeague class.
     *
     * @param maxAge the maximum age of the players in this league
     * @param sex the sex of the players in this league
     * @param nameOfLeague name of the league
     */
    public PlayerLeague(int maxAge, char sex, String nameOfLeague){
        this.setMaxAge(maxAge);
        this.sex = sex;setSex(sex);
        this.setNameOfLeague(nameOfLeague);
        this.players = new ArrayList<>();
    }


    /**
     * Set the name of this league. If the name is blank or empty,
     * the name of the league will be set to predefined value.
     *
     * @param nameOfLeague the name of the league
     */
    private void setNameOfLeague(String nameOfLeague){
        if(nameOfLeague.isEmpty() || nameOfLeague.isBlank()){
            this.nameOfLeague = "Invalid name";
        }else{
            this.nameOfLeague = nameOfLeague.strip().toLowerCase(Locale.ROOT);
        }
    }

    /**
     * Set the max age og this league.
     *
     * @param maxAge the maximum age of the players in this class
     */
    private void setMaxAge(int maxAge){
        this.maxAge = maxAge;
    }

    /**
     * Set the sex of this class.
     *
     * @param sex the sex of this class, M for male and W for woman
     */
    private void setSex(char sex){
        this.sex = sex;
    }

    /**
     * Add a player to the league.
     *
     * @param player the player you want to add
     * @throws PlayerTooOldException if the player is older than the maximum age
     */
    public void addPlayer(Player player) throws PlayerTooOldException {
        if(player.getAge()<=this.maxAge){
            this.players.add(player);
        }else{
            throw new PlayerTooOldException(0);
        }
    }

    /**
     * Add multiple players to this league.
     *
     * @param players a list of players you want to add
     * @throws PlayerTooOldException throws if one of the players in the list is too old
     */
    public void addMultiplePlayers(List<Player> players) throws PlayerTooOldException{
        for (Player p:players) {
            if (p.getAge()<=maxAge){
                this.players.add(p);
            }else{
                throw new PlayerTooOldException(players.indexOf(p));
            }
        }
    }
}
