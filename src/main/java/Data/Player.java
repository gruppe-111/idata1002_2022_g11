package Data;

import java.util.Locale;

/**
 * A class to represent player in the game badminton. And some basic methodes to use this player.
 *
 * @author Jakob Finstad
 * @version 18.03.2022
 */
public class Player {
    private String firstName;
    private String lastName;
    private int age;
    private String nationality;
    private int matchesPlayed;
    private int matchesWon;

    /**
     * Create a player with given information.
     *
     * @param firstName the first name of the player
     * @param lastName the last name of the player
     * @param age age og the player, has to be 11 or above, else throws
     * @param nationality the nationality of the player
     * @param matchesPlayed the amount of matches this player have played
     * @param matchesWon the amount of matches this player has won
     */
    public Player(String firstName, String lastName, int age, String nationality,int matchesPlayed, int matchesWon){
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAge(age);
        this.setNationality(nationality);
        this.setMatchesPlayed(matchesPlayed);
        this.setMatchesWon(matchesWon);
    }

    /**
     * Get first name of the player.
     *
     * @return first name of the player
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get the last name of the player.
     *
     * @return last name of the player
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Get the age of the player.
     *
     * @return age of the player
     */
    public int getAge() {
        return age;
    }


    /**
     * Get nationality of the player.
     *
     * @return nationality of the player
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Get matches played by this player.
     *
     * @return the amount of matches played by the player
     */
    public int getMatchesPlayed(){
        return this.matchesPlayed;
    }


    /**
     * Get matches won by the player.
     *
     * @return the amount of matches won by the player
     */
    public int getMatchesWon(){
        return this.matchesWon;
    }

    /**
     * Set the firstname of the player, strips and sets the string to lowercase.
     *
     * @param firstName first name of the player
     */
    protected void setFirstName(String firstName) {
        if(firstName!=null && !firstName.isEmpty()) {
            String newFirstName = firstName.substring(0, 1).strip().toUpperCase(Locale.ROOT) + firstName.substring(1).strip().toLowerCase();
            this.firstName = newFirstName.strip();
        }else{
            this.firstName = "Invalid name";
        }

    }

    /**
     * Set the lastname of the player, strips and sets the string to lowercase.
     *
     * @param lastName lastname of the player
     */
    protected void setLastName(String lastName) {
        if(lastName!=null && !lastName.isEmpty()) {
            String newLastName = lastName.substring(0, 1).strip().toUpperCase(Locale.ROOT) + lastName.substring(1).strip().toLowerCase();

            this.lastName = newLastName.strip();
        }else{
            this.lastName = "Invalid Name";
        }
    }

    /**
     * Set the age of the player.
     *
     * @param age age of the player, have to be 11 or older
     */
    public void setAge(int age){
        if(age>=11){
            this.age = age;
        }else{
            this.age = 11;
        }
    }


    /**
     * Set the nationality of the player, strips and sets the string to lowercase.
     *
     * @param nationality nationality of the player
     */
    protected void setNationality(String nationality) {
        if(nationality!=null && !nationality.isEmpty()) {
            String newNationality = nationality.substring(0, 1).strip().toUpperCase(Locale.ROOT) + nationality.substring(1).strip().toLowerCase();
            this.nationality = newNationality.strip();
        }else{
            this.nationality = "Invalid nationality";
        }
    }

    /**
     * Set the matchesPlayed field for this player. If the player has
     * fewer than zero, the value will be set to 0.
     *
     * @param matchesPlayed the amount of played matches, not below zero
     */
    private void setMatchesPlayed(int matchesPlayed){
        if(matchesPlayed<0){
            this.matchesPlayed = 0;
        }else{
            this.matchesPlayed = matchesPlayed;
        }
    }

    /**
     * Set the matchesWon field for this player. Cannot be below zero
     * else it will be set to zero by default.
     *
     * @param matchesWon the amount of matches won, cannot be below zero
     */
    private void setMatchesWon(int matchesWon) {
        if(matchesWon<0){
            this.matchesWon = 0;
        }else{
            this.matchesWon = matchesWon;
        }
    }

    /**
     * Basic toString for debugging.
     *
     * @return a standard format for debugging
     */
    @Override
    public String toString() {
        return "Player{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", nationality='" + nationality + '\'' +
                '}';
    }

    public void addMatch(boolean matchWon){
        this.matchesPlayed++;
        if (matchWon){
            this.matchesWon++;
        }
    }
}