package Data;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A match with players, location, time etc.
 *
 * @author Jakob Finstad
 * @version 25.03.2022
 */
public class Match {
    private List<Player> players;
    private String placePlayed;
    private String timeOfMatch;
    private PlayerLeague playerClass;
    private int matchNumber;

    // Dokumentasjon her, og adding av time of match må fikses

    /**
     * Constructor for a match. Contains place for the match, time, 2 players(and reserves) and the league.
     *
     * @param placePlayed the place this match was played at
     * @param timeOfMatch the time the match was played in format '01.02.2022 20:10'
     * @param playerClass the class this match is played in
     * @param matchNumber the match number, which indicates what match this is
     */
    public Match(String placePlayed, String timeOfMatch, PlayerLeague playerClass, int matchNumber){
        this.setPlacePlayed(placePlayed);
        this.timeOfMatch = timeOfMatch;
        this.setPlayerClass(playerClass);
        this.players = new ArrayList<>();
        this.matchNumber = matchNumber;
    }

    /**
     * Set this match place. If the place is blank or empty,
     * the place will be set to predefined value.
     *
     * @param placePlayed the place of the match, cannot be empty
     */
    private void setPlacePlayed(String placePlayed){
        if (placePlayed.isEmpty() || placePlayed.isBlank()){
            this.placePlayed = "Invalid place";
        }else{
            this.placePlayed = placePlayed.strip().toLowerCase(Locale.ROOT);
        }
    }

    /**
     * Set the playerClass of this match.
     *
     * @param playerClass the PlayerLeague this match belongs to
     */
    private void setPlayerClass(PlayerLeague playerClass){
        //Må legges til exception handling her
        this.playerClass = playerClass;
    }

    /**
     * Add a player to the match.
     * @param player a player that contribute in this match
     */
    public void addPlayer(Player player){
        this.players.add(player);
    }

    /**
     * Add multiple players to the match.
     * @param players list of players that contributed to this match
     */
    public void addMultiplePlayers(List<Player> players){
        this.players.addAll(players);
    }

    /**
     * Get players to this match.
     *
     * @return a list of players that contributed in the match
     */
    public List<Player> getPlayers(){
        return this.players;
    }

    /**
     * Get the date and time for the match.
     * @return date and time of the match
     */
    public String getTimeOfMatch() {
        return timeOfMatch;
    }

    /**
     * Get the player league this match belongs to.
     *
     * @return the player league
     */
    public PlayerLeague getPlayerClass() {
        return playerClass;
    }

    /**
     * Get the place this match was played.
     *
     * @return the place of the match was played
     */
    public String getPlacePlayed() {
        return placePlayed;
    }


    /**
     * Get the number of this match.
     *
     * @return the number of the match
     */
    public int getMatchNumber(){
        return this.matchNumber;
    }
}
