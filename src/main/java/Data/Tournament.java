package Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A class to handle the over all tournament. With some basic interactions.
 *
 * @author Jakob Finstad
 * @version 25.04.2022
 */
public class Tournament {
    private List<Team> teams;
    private List<PlayerLeague> leagues;
    private List<Match> matches;

    /**
     * Constructor for the class.
     */
    public Tournament(){
        this.teams = new ArrayList<>();
        this.leagues = new ArrayList<>();
        this.matches = new ArrayList<>();
    }

    /**
     * Get the teams of this tournament.
     *
     * @return the teams of this tournament
     */
    public List<Team> getTeams(){
        return this.teams;
    }


    /**
     * Get the leagues in this tournament.
     *
     * @return the leagues in this tournament
     */
    public List<PlayerLeague> getLeagues(){
        return this.leagues;
    }


    /**
     * Get matches in this tournament.
     *
     * @return the matches registered in this tournament
     */
    public List<Match> getMatches(){
        return this.matches;
    }


    /**
     * Add one team to the tournament.
     *
     * @param tm team that shall be added
     */
    public void addTeam(Team tm){
        this.teams.add(tm);
    }


    /**
     * Add multiple teams to the tournament.
     *
     * @param teams a list with teams that shall be added to tournament
     */
    public void addMultipleTeams(List<Team> teams){
        for(Team tm:teams){
            this.teams.add(tm);
        }
    }


    /**
     * Add league to the tournament.
     *
     * @param pl the league that shall be added
     */
    public void addLeague(PlayerLeague pl){
        this.leagues.add(pl);
    }


    /**
     * Add multiple leagues to this tournament.
     *
     * @param pls list with leagues that shall be added
     */
    public void addMultipleLeagues(List<PlayerLeague> pls){
        for(PlayerLeague pl:pls){
            this.leagues.add(pl);
        }
    }

    /**
     * Add a player to a team.
     *
     * @param tm the team that the player should be added to
     * @param firstName firstname of the player
     * @param lasName lastname of the player
     * @param age the age of the player
     * @param nationality origin of the player
     * @param matchesPlayed matches played by this player
     * @param matchesWon matches won by this player
     */
    public void addNewPlayerTooTeam(Team tm,String firstName, String lasName, int age, String nationality, int matchesPlayed, int matchesWon){
        Player player = new Player(firstName,lasName,age,nationality,matchesPlayed,matchesWon);
        tm.addMember(player);
    }


    /**
     * Add match to the tournament.
     *
     * @param placePlayed place played
     * @param playerLeague the player league of this match
     * @param matchNumber number which indicates with match it is
     */
    public void addMatch(String placePlayed,String timeOfMatch, PlayerLeague playerLeague,int matchNumber){
        Match m = new Match(placePlayed,timeOfMatch, playerLeague, matchNumber);
        this.matches.add(m);
    }


    /**
     * Add multiple matches to the tournament.
     *
     * @param matches list with matches that shall be added
     */
    public void addMultipleMatches(List<Match> matches){
        for(Match m:matches){
            this.matches.add(m);
        }
    }


    /**
     * Search player by their name. Not caps sensitive.
     *
     * @param name the name of the player you want to search for
     * @return the found player, returns null if no player was found
     */
    public Player searchPlayerByFirstName(String name){
        Player playerFound = null;
        name = name.strip().toLowerCase(Locale.ROOT);
        String pName = "";
/*
        for (Player p:this.players){
            pName = p.getFirstName().toLowerCase(Locale.ROOT).strip();
            if(pName.equals(name)){
                 playerFound = p;
            }
        }
**/
        return playerFound;
    }


    /**
     * Search a team by the name. Not caps sensitive.
     *
     * @param name name of the team you want to search
     * @return the team that has the same name, or null if no team was found
     */
    public Team searchTeamByName(String name){
        Team teamFound = null;
        name = name.toLowerCase(Locale.ROOT).strip();
        String tName = "";

        for (Team tm:this.teams){
            tName = tm.getName().toLowerCase(Locale.ROOT).strip();
            if (tName.equals(name)){
                teamFound = tm;
            }
        }

        return teamFound;
    }


    /**
     * Get the match by their match number. Returns null if no match was found.
     *
     * @param integer the match number of the match you want to find
     * @return the match if it was found, else it returns null
     */
    public Match searchMatchByMatchNumber(int integer){
        Match matchFound = null;
        for (Match m:this.matches){
            if(m.getMatchNumber()==integer){
                matchFound = m;
            }
        }
        return matchFound;
    }

    public Team getTeamByIndex(int index){
        return this.teams.get(index);
    }

}
