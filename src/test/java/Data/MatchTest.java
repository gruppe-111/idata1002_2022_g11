package Data;

import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MatchTest {

    Player player1 = new Player("Test","Testesen",0,"",10,5);
    Player player2 = new Player("Test","Testesen",0,"",10,5);
    Player player3 = new Player("Test","Testesen",0,"",10,5);
    Player player4 = new Player("Test","Testesen",0,"",10,5);

    List<Player> list1 = new ArrayList<>();
    List<Player> list2 = new ArrayList<>();
    List<Player> list3 = new ArrayList<>();

    PlayerLeague playerLeague1 = new PlayerLeague(20,'M',"U20");
    Match match1 = new Match("Sky", "02.02.2022 20:20", playerLeague1,1);
    Match match2 = new Match("Oslo", "02.02.2022 20:20", playerLeague1,2);


    private void addToList(){
        list1.add(player1);
        list1.add(player2);

        list2.add(player1);
        list2.add(player2);
        list2.add(player3);

        list3.add(player1);
        list3.add(player2);
        list3.add(player3);
        list3.add(player4);
    }

    @Test
    public void addPlayerTest(){
        addToList();
        assertEquals(0,match1.getPlayers().size());
        match1.addPlayer(player1);
        assertEquals(1,match1.getPlayers().size());
        match2.addMultiplePlayers(list1);
        assertEquals(2,match2.getPlayers().size());
    }

    @Test
    public void addPlayerClassTest(){
        assertEquals(playerLeague1,match1.getPlayerClass());
    }

}
