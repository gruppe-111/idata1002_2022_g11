package Data;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PlayerTest {
    Player player1 = new Player("test", "test",16,"norway",10,3);
    Player player3 = new Player("test", "testnes",16,"norway",10,8);
    Player player2 = new Player("test", "testesen",16,"norway",10,1);

    @Test
    public void setFirstNameTest(){
        assertEquals("Test",player1.getFirstName());
        player1.setFirstName(null);
        assertEquals("Invalid name",player1.getFirstName());

    }

    @Test
    public void setLastNameTest(){
        assertEquals("Test",player1.getLastName());
        player1.setLastName(null);
        assertEquals("Invalid Name",player1.getLastName());
    }

    @Test
    public void setNationalityTest(){
        assertEquals("Norway",player1.getNationality());
        player1.setNationality(null);
        assertEquals("Invalid nationality",player1.getNationality());
    }

    @Test
    public void addMatchTest(){
        assertEquals(10,player2.getMatchesPlayed());
        assertEquals(1,player2.getMatchesWon());

        player2.addMatch(true);
        assertEquals(11,player2.getMatchesPlayed());
        assertEquals(2,player2.getMatchesWon());

        player2.addMatch(false);
        assertEquals(12,player2.getMatchesPlayed());
        assertEquals(2,player2.getMatchesWon());
    }

    @Test
    public void setAgeTest(){
        assertEquals(16,player3.getAge());

        player3.setAge(7);
        assertEquals(11,player3.getAge());

        player3.setAge(14);
        assertEquals(14,player3.getAge());
    }
}
